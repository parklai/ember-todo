EmberTodo.Router.map(function () {
  this.resource('embertodo', {path: '/'}, function(){
    this.route('active');
    this.route('completed');
  });
});

EmberTodo.EmbertodoRoute = Ember.Route.extend({
  model: function(){
    return this.store.find('todo');
  }
});

EmberTodo.EmbertodoIndexRoute = Ember.Route.extend({
  model: function(){
    return this.modelFor('embertodo');
  }
});

EmberTodo.EmbertodoActiveRoute = Ember.Route.extend({
  model: function(){
    return this.store.filter('todo', function(todo){
      return !todo.get('isCompleted');
    });
  },
  renderTemplate: function(controller){
    this.render('embertodo/index', {controller: controller});
  }
});

EmberTodo.EmbertodoCompletedRoute = Ember.Route.extend({
  model: function(){
    return this.store.filter('todo', function(todo){
      return todo.get('isCompleted');
    });
  },
  renderTemplate: function(controller){
    this.render('embertodo/index', {controller: controller});
  }
});